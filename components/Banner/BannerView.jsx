import React from "react";
import Button from "../Button";
import SignInIcon from "../../icons/sign-in.svg";

const BannerView = (props) => {
  const baseClass = "banner";

  return (
    <section id={props.id} className={`${baseClass}`}>
      <div className={`${baseClass}__opacity`} />
      <div className={`${baseClass}__background`}>
        <img src={props.mainImage || "https://picsum.photos/1900"} alt="Imagen banner" />
      </div>
      <Button
        className={`${baseClass}__button`}
        external
        href="https://www.toornament.com/es/tournaments/3469004331731492864/information"
        icon={<SignInIcon />}
        value="Inscríbete Ahora"
      />
    </section>
  );
};

export default BannerView;
