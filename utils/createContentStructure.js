const fs = require('fs')

const createContentStructure = (downloadedContent) => {
  return new Promise((resolve, reject) => {
    fs.mkdir('content', { recursive: true }, (err) => {
      for (const content in downloadedContent) {
        if (downloadedContent.hasOwnProperty(content)) {

          if (content === "siteConfig") {
            fs.writeFile(
              `content/${content}.json`,
              JSON.stringify(downloadedContent[content].data, null, 3),
              (err) => {
                if (err) throw err;
                console.log(`${content}`);
              }
            );
          }

          if (content === "mainMenu") {
            fs.writeFile(
              `content/${content}.json`,
              JSON.stringify(downloadedContent[content], null, 3),
              (err) => {
                if (err) throw err;
                console.log(`${content}`);
              }
            );
          }
        }
      }
    })

    resolve(true)
  })
}

module.exports = createContentStructure;
