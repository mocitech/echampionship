const getContentSlug = (contentType, contentURL) => {
  let normalizedContentType = contentType

  normalizedContentType = normalizedContentType.replace('á', 'a')
  normalizedContentType = normalizedContentType.replace('é', 'e')
  normalizedContentType = normalizedContentType.replace('í', 'i')
  normalizedContentType = normalizedContentType.replace('ó', 'o')
  normalizedContentType = normalizedContentType.replace('ú', 'u')
  normalizedContentType = normalizedContentType.replace(' ', '-')

  return contentURL.replace(`/campeonato-estrellas/web/${normalizedContentType.toLowerCase()}`, "")
}

module.exports = getContentSlug