import Head from "next/head";
import Header from "../components/Header";
import BannerCarousel from "../components/BannerCarousel";
import Banner from "../components/Banner";

const Home = (props) => {
  return (
    <div>
      <Head>
        <title>{props.appName} | Inicio</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Header mainMenu={props.mainMenu} />
      <main>
        {!props.isSiteActive && <Banner mainImage="/banner-landing.webp" />}
        {props.isSiteActive && (
          <>
            {props.partidos.length === 0 && <Banner mainImage="/banner-landing.webp" />}
            {props.partidos.length > 0 && <BannerCarousel matches={props.partidos} />}
          </>
        )}
      </main>
    </div>
  );
};

export const getStaticProps = async () => {
  const fetch = require("isomorphic-fetch");
  const { APP_NAME, LAMBDA_URL } = process.env;

  try {
    const response = await fetch(
      `${LAMBDA_URL}/.netlify/functions/get-api-content`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      }
    );

    if (!response.ok) {
      if (response.status === 422) {
        throw new Error(`Error ${JSON.stringify(data)}`);
      } else {
      }
      return;
    }

    const data = await response.json();

    return {
      props: {
        appName: APP_NAME,
        isSiteActive: data.msg.siteConfig.data.is_site_active === 0 ? false : true,
        mainMenu: data.msg.mainMenu,
        partidos: data.msg.partidos,
        pageName: "Inicio",
      },
    };

  } catch (err) {
    console.log("There was an error", err);
  }
};

export default Home;
