import React from "react";
import getImageURL from "../../utils/getImageURL";
import Countdown from "../Countdown";
import ArrowLeft from "../../icons/arrow-left-thin.svg";
import ArrowRight from "../../icons/arrow-right-thin.svg";
import Button from "../Button";
import PlayCircle from "../../icons/play-circle.svg";

const Slide = (props) => {
  const baseClass = "slide";

  const homeStarName = props.homeStar.split();
  const awayStarName = props.awayStar.split();

  return (
    <div className="slide">
      <img className="slide__background" src={props.mainImage} alt="" />
      <div className="slide__content-wrapper">
        {Date.parse(props.date) < Date.now() && (
          <Button
            icon={<PlayCircle />}
            className="slide__button"
            rounded
            value="Live Streaming"
          />
        )}

        <Countdown className={`${baseClass}__countdown`} date={props.date} />

        <div className={`${baseClass}__celebrities-names-wrapper`}>
          <div className={`${baseClass}__celebrity-name`}>
            <h2>
              {homeStarName[0]} <span>{homeStarName[1]}</span>
            </h2>
          </div>
          <div className={`${baseClass}__celebrity-name`}>
            <h2>
              {awayStarName[0]} <span>{awayStarName[1]}</span>
            </h2>
          </div>
        </div>
      </div>
    </div>
  );
};

const BannerCarouselView = (props) => {
  const baseClass = "banner-carousel";

  // console.log(props.matches);

  // return null

  return (
    <div className={`${baseClass}`}>
      <div className={`${baseClass}__opacity`}></div>
      <div className={`${baseClass}__slider`} ref={props.carouselSliderEl}>
        {props.matches.map((match) => {
          return (
            <Slide
              key={`${match.contentType}-${match.id}`}
              homeStar={match.home}
              awayStar={match.away}
              date={match.date}
              mainImage={getImageURL(match.mainImage)}
            />
          );
        })}
      </div>

      <ArrowLeft
        className={`${baseClass}__left-arrow`}
        onClick={props.handleArrowClick}
      />

      <ArrowRight
        className={`${baseClass}__right-arrow`}
        onClick={(e) => props.handleArrowClick(e, true)}
      />
    </div>
  );
};

export default BannerCarouselView;
