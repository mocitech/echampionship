import React from "react";
import Link from "next/link";
import Button from "../Button";
import HandIcon from "../../icons/hand.svg";
import StarIcon from "../../icons/star.svg";
import SignInIcon from "../../icons/sign-in.svg";
import HamburgerIcon from "../../icons/hamburger.svg";
import normalizeMenuLink from "../../utils/normalizerMenuLink";
import SECONDARY_MENU_ITEMS from "../../utils/constants/secondaryMenuItems";

const HeaderView = (props) => {
  const baseClass = "header";

  return (
    <header className={`${baseClass}`}>
      {props.isLoading ? (
        <h4 style={{ color: "white" }}>Cargando</h4>
      ) : (
        <div className={`${baseClass}__wrapper`}>
          <div className={`${baseClass}__logo-wrapper`}>
            <Link href="/">
              <a aria-label="Estrellas PES logo">
                <img
                  className={`${baseClass}__logo`}
                  src="/logo.png"
                  alt="Estrellas PES logo"
                />
              </a>
            </Link>

            <HamburgerIcon className={`${baseClass}__hamburger-icon`} />
          </div>
          <div className={`${baseClass}__navs-wrapper`}>
            <nav className={`${baseClass}__secondary-nav`}>
              <ul className={`${baseClass}__secondary-nav__list`}>
                <li
                  className={`
                    ${baseClass}__secondary-nav__list-item
                    ${
                      props.menuItemSelected === SECONDARY_MENU_ITEMS.ESTRELLAS
                        ? "active"
                        : "inactive"
                    }
                  `}
                  onClick={() =>
                    props.handleSecondaryMenuChange(
                      SECONDARY_MENU_ITEMS.ESTRELLAS
                    )
                  }
                >
                  <StarIcon className={`${baseClass}__secondary-nav__icon`} />
                  <p className={`${baseClass}__secondary-nav__label`}>
                    Estrellas
                  </p>
                </li>
                <li
                  className={`
                    ${baseClass}__secondary-nav__list-item
                    ${
                      props.menuItemSelected === SECONDARY_MENU_ITEMS.FANS
                        ? "active"
                        : "inactive"
                    }
                  `}
                  onClick={() =>
                    props.handleSecondaryMenuChange(SECONDARY_MENU_ITEMS.FANS)
                  }
                >
                  <HandIcon className={`${baseClass}__secondary-nav__icon`} />
                  <p className={`${baseClass}__secondary-nav__label`}>Fans</p>
                </li>
              </ul>
              <div className={`${baseClass}__secondary-nav__button-wrapper`}>
                <Button
                  external
                  href="https://www.toornament.com/es/tournaments/3469004331731492864/information"
                  icon={<SignInIcon />}
                  size="medium"
                  value="Inscríbete al Torneo"
                />
              </div>
            </nav>
            <nav className={`${baseClass}__nav`}>
              <ul className={`${baseClass}__list`}>
                {props.mainMenu.map((menuItem, index) => {
                  return (
                    <li
                      data-nav-index={index}
                      className={`${baseClass}__list__item ${
                        props.activeItem === index ? "active" : ""
                      }`}
                      key={menuItem.key}
                      onMouseEnter={() => props.setActiveItem(index)}
                      onMouseLeave={() => props.setActiveItem(null)}
                    >
                      {menuItem.below && (
                        <ul className={`${baseClass}__inner-list`}>
                          {menuItem.below.map((childItem) => {
                            return (
                              <li
                                className={`${baseClass}__inner-list__item`}
                                key={childItem.key}
                              >
                                <div>
                                  <Link
                                    href={`/${
                                      props.menuItemSelected
                                    }/${normalizeMenuLink(childItem.absolute)}`}
                                  >
                                    <a>{childItem.title}</a>
                                  </Link>
                                </div>
                              </li>
                            );
                          })}
                        </ul>
                      )}
                      <div
                        className={`${baseClass}__inner-list__main-item`}
                        onClick={() => {
                          if (props.activeItem === index) {
                            props.setActiveItem(null);
                          } else {
                            props.setActiveItem(index);
                          }
                        }}
                      >
                        {menuItem.below ? (
                          menuItem.title
                        ) : (
                          <Link
                            href={`/${normalizeMenuLink(menuItem.absolute)}`}
                          >
                            <a>{menuItem.title}</a>
                          </Link>
                        )}
                      </div>
                    </li>
                  );
                })}
              </ul>
            </nav>
          </div>
        </div>
      )}
    </header>
  );
};

export default React.memo(HeaderView);
