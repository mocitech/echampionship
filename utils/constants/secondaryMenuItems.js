const SECONDARY_MENU_ITEMS = {
  ESTRELLAS: "estrellas",
  FANS: "fans",
};

export default SECONDARY_MENU_ITEMS;
