const { API_URL } = process.env;

const normalizeMenuLink = (link) => {
  return link.replace(`${API_URL}/`, "");
};

export default normalizeMenuLink;
