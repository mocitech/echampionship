import React from "react";
import Header from "../Header";
import Drawer from "../Drawer";

const BaseLayoutView = (props) => {
  const baseClass = "base-layout";

  return (
    <div className={`${baseClass}`}>
      <Drawer />
      <Header />
      <main>{props.children}</main>
    </div>
  );
};

export default BaseLayoutView;
