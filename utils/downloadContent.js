require("dotenv").config();
const fetch = require("isomorphic-fetch");
const fs = require("fs");
const getContentSlug = require("./getContentSlug");
const CONTENT_ENDPOINTS = require("./constants/contentEndpoints");

const getContent = (contentName, contentEndpoint) => {
  return new Promise(async (resolve, reject) => {
    fs.mkdir("content", { recursive: true }, (err) => {});

    if (contentName === "siteConfig") {
      const res = await fetch(contentEndpoint);
      const content = await res.json();

      fs.writeFile(
        `content/${contentName}.json`,
        JSON.stringify(content.data, null, 3),
        (err) => {
          if (err) throw err;
          console.log(`${contentName}`);
        }
      );

      return;
    }

    if (contentName === "mainMenu") {
      const res = await fetch(contentEndpoint);
      const content = await res.json();

      fs.writeFile(
        `content/${contentName}.json`,
        JSON.stringify(content, null, 3),
        (err) => {
          if (err) throw err;
          console.log(`${contentName}`);
        }
      );

      return;
    }

    const folderName = `content/${contentName}`;

    const res = await fetch(contentEndpoint);
    const contents = await res.json();

    fs.mkdir(folderName, { recursive: true }, (err) => {
      if (err) throw err;

      contents.forEach((content) => {
        const slug = getContentSlug(content.type, content.url);

        fs.writeFile(
          `${folderName}${slug}.json`,
          JSON.stringify(content, null, 3),
          (err) => {
            if (err) throw err;
            console.log(`${contentName}${slug}`);
          }
        );
      });
    });
  });
};

const main = async () => {
  const contentPromises = [];

  for (const content in CONTENT_ENDPOINTS) {
    if (CONTENT_ENDPOINTS.hasOwnProperty(content)) {
      const endpoint = CONTENT_ENDPOINTS[content];

      contentPromises.push(getContent(content, endpoint));
    }
  }

  await Promise.all(contentPromises);
};

main();
