const { API_URL } = process.env;

CONTENT_ENDPOINTS = {
  mainMenu: `${API_URL}/api/menu_items/main-menu`,
  siteConfig: `${API_URL}/api/get-site-config`,
};

module.exports = CONTENT_ENDPOINTS;
