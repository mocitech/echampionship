import React from "react";
import Link from "next/link";

const DrawerView = (props) => {
  const baseClass = "drawer";
  const isOpen = props.isOpen ? `${baseClass}--open` : "";

  return (
    <nav className={`${baseClass} ${isOpen} ${props.className}`}>
      <div className={`${baseClass}__spacing`} onClick={props.onClickOutside} />
      <ul className="drawer__list">
        <li className="drawer__item">
          <Link href="/#quienes-somos">
            <a className="header__nav__link">Quiénes Somos</a>
          </Link>
        </li>
        <li className="drawer__item">
          <Link href="/#servicios">
            <a className="header__nav__link">Servicios</a>
          </Link>
        </li>
        <li className="drawer__item">
          <Link href="/#contactenos">
            <a className="header__nav__link">Contáctenos</a>
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default DrawerView;
