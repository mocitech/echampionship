require("dotenv").config();

const { API_URL, APP_NAME, BACKEND_URL } = process.env;

module.exports = {
  env: {
    API_URL,
    APP_NAME,
    BACKEND_URL,
  },
};
