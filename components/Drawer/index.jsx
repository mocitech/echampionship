import React from "react";
import DrawerView from "./DrawerView";

const Drawer = (props) => {
  return <DrawerView {...props} />;
};

export default Drawer;
