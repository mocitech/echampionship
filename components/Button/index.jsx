import React from "react";
import ButtonView from "./ButtonView";

const Button = (props) => {
  return <ButtonView {...props} />;
};

export default Button;
