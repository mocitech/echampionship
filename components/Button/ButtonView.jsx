import React from "react";

const ButtonView = (props) => {
  const baseClass = "button";
  const rounded = props.rounded ? "rounded" : "";
  const size = props.size || "large";

  if (props.href && props.external) {
    return (
      <a
        href={props.href}
        className={`${baseClass} ${rounded} ${size} ${props.className}`}
      >
        {props.icon &&
          React.cloneElement(props.icon, { className: `${baseClass}__icon` })}
        {props.value}
      </a>
    );
  }

  return (
    <button
      disabled={props.disabled}
      className={`${baseClass} ${rounded} ${size} ${props.className}`}
    >
      {props.icon &&
        React.cloneElement(props.icon, { className: `${baseClass}__icon` })}
      <p className={`${baseClass}__value`}>{props.value}</p>
    </button>
  );
};

export default ButtonView;
