import React, { useContext, useEffect, useState } from "react";
import HeaderView from "./HeaderView";
import GlobalContext from "../../contexts/GlobalContext";

const Header = (props) => {
  const globalContext = useContext(GlobalContext);

  const [isLoading, setIsLoading] = useState(false);
  const [activeItem, setActiveItem] = useState(null);

  useEffect(() => {
    if (globalContext.menus.main) {
      setIsLoading(false);
    }
  }, [globalContext.menus.main]);

  const handleSecondaryMenuChange = (menuItem) => {
    globalContext.setMenuItemSelected(menuItem);
  };

  return (
    <HeaderView
      activeItem={activeItem}
      handleSecondaryMenuChange={handleSecondaryMenuChange}
      isLoading={isLoading}
      mainMenu={props.mainMenu}
      menuItemSelected={globalContext.menuItemSelected.toLowerCase()}
      setActiveItem={setActiveItem}
    />
  );
};

export default React.memo(Header);
