const { BACKEND_URL } = process.env;

const getImageURL = (url) => {
  return `${BACKEND_URL}${url}`;
};

module.exports = getImageURL;
