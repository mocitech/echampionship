import React, { useEffect, useState } from "react";
import GlobalContext from "./GlobalContext";
import SECONDARY_MENU_ITEMS from "../utils/constants/secondaryMenuItems";
const { API_URL } = process.env;

require("isomorphic-fetch");

const GlobalProvider = (props) => {
  const [menus, setMenus] = useState({
    main: null,
  });

  const [menuItemSelected, setMenuItemSelected] = useState(
    SECONDARY_MENU_ITEMS.ESTRELLAS
  );

  const [screenSize, setScreenSize] = useState(null);

  useEffect(() => {
    // const getMainMenu = () => {
    //   fetch(
    //     `${API_URL}/api/menu_items/main-menu`
    //   )
    //     .then((response) => {
    //       return response.json();
    //     })
    //     .then((main) => {
    //       setMenus((menus) => {
    //         return {
    //           ...menus,
    //           main,
    //         };
    //       });
    //     });
    // };

    // !menus.main && getMainMenu();

    document && setScreenSize(document.body.offsetWidth);
  }, []);

  const defaultValue = {
    menuItemSelected,
    menus,
    screenSize,
    setMenuItemSelected,
    setMenus,
  };

  return (
    <GlobalContext.Provider value={defaultValue}>
      {props.children}
    </GlobalContext.Provider>
  );
};

export default GlobalProvider;
