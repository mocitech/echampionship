import React from "react";
import BannerView from "./BannerView";

const Banner = (props) => {
  return <BannerView {...props} />;
};

export default Banner;
