import GlobalProvider from "../contexts/GlobalProvider";
import "../styles/main.scss";

const CustomApp = ({ Component, pageProps }) => {
  return (
    <GlobalProvider>
      <Component {...pageProps} />
    </GlobalProvider>
  );
};

export default CustomApp;
