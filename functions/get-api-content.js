const fetch = require("node-fetch");

exports.handler = async function (event, context) {
  const { API_URL } = process.env;

const ENDPOINTS = {
  mainMenu: `${API_URL}/api/menu_items/main-menu`,
  siteConfig: `${API_URL}/api/get-site-config`,
  // noticias: `${API_URL}/noticias`,
  // estrellas: `${API_URL}/estrellas`,
  partidos: `${API_URL}/partidos`,
}

  const getContent = (contentName, contentEndpoint) => {
    return new Promise(async (resolve, reject) => {
      const res = await fetch(`${contentEndpoint}`, {
        headers: { Accept: "application/json" },
      });

      const content = await res.json();

      resolve({
        [contentName]: content,
      });
    });
  };

  /**
   * reponse = [ { mainMenu: [Array] }, { siteConfig: [Object] }, ... ]
   */
  const parseResponse = (response) => {
    let parsedResponse = {};

    response.forEach((responseElement) => {
      const key = Object.keys(responseElement)[0];
      const value = responseElement[key];

      parsedResponse = {
        ...parsedResponse,
        [key]: value,
      };
    });

    return parsedResponse;
  };

  try {
    let contentPromises = [];

    for (const content in ENDPOINTS) {
      if (ENDPOINTS.hasOwnProperty(content)) {
        const endpoint = ENDPOINTS[content];

        contentPromises.push(getContent(content, endpoint));
      }
    }

    const response = await Promise.all(contentPromises);

    return {
      statusCode: 200,
      body: JSON.stringify({ msg: parseResponse(response) }),
    };

    // Hola

    // if (!response.ok) {
    //   // NOT res.status >= 200 && res.status < 300
    //   return { statusCode: response.status, body: response.statusText };
    // }
  } catch (err) {
    console.log(err); // output to netlify function log

    return {
      statusCode: 500,
      body: JSON.stringify({ msg: err.message }), // Could be a custom message or object i.e. JSON.stringify(err)
    };
  }
};
