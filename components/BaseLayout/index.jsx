import React from "react";
import BaseLayoutView from "./BaseLayoutView";

const BaseLayout = (props) => {
  return <BaseLayoutView {...props} />;
};

export default BaseLayout;
