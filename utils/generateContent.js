const fetch = require("isomorphic-fetch");

const generateContent = () => {
  return new Promise(async (resolve, reject) => {
    const { LAMBDA_URL } = process.env;

    try {
      const response = await fetch(
        `${LAMBDA_URL}/.netlify/functions/get-api-content`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
        }
      );

      if (!response.ok) {
        if (response.status === 422) {
          throw new Error(`Error ${JSON.stringify(data)}`);
        } else {
        }
        return;
      }

      const data = await response.json();

      resolve(data.msg);
    } catch (err) {
      console.log("There was an error", err);
    }
  });
};

module.exports = generateContent;
