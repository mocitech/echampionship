import React, { useRef, useState } from "react";
import BannerCarouselView from "./BannerCarouselView";

const BannerCarousel = (props) => {
  const carouselSliderEl = useRef(null);

  const [activeSlide, setActiveSlide] = useState(1);

  const handleArrowClick = (e, right) => {
    if (activeSlide === 1 && !right) {
      return;
    }

    if (right && activeSlide > 2) {
      return;
    }

    carouselSliderEl.current.style.transition = "transform .4s ease-in-out";

    const screenSize = document.body.offsetWidth;

    setActiveSlide(right ? activeSlide + 1 : activeSlide - 1);

    let movement = -(activeSlide * screenSize);

    carouselSliderEl.current.style.transform = `translateX(${movement}px)`;
  };

  return (
    <BannerCarouselView
      activeSlide={activeSlide}
      carouselSliderEl={carouselSliderEl}
      handleArrowClick={handleArrowClick}
      {...props}
    />
  );
};

export default BannerCarousel;
